use std::{
    collections::VecDeque,
    io::{self, Read, Write},
    net::{TcpListener, TcpStream},
    sync::{Arc, Mutex},
    thread,
};

struct Bundle {
    stream: TcpStream,
    queue: Arc<Mutex<VecDeque<u8>>>,
}

fn main() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8570")?;

    // Create a queue that can be shared safely across threads
    let queue = Arc::new(Mutex::new(VecDeque::new()));

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let queue = queue.clone();
                thread::spawn(move || handle_client(Bundle { stream, queue }));
            }
            _ => continue,
        }
    }

    Ok(())
}

fn handle_client(mut bundle: Bundle) {
    let mut buf = [0; 1];

    match bundle.stream.read_exact(&mut buf) {
        Ok(_) if buf[0] == 0 => poster(bundle),
        Ok(_) if buf[0] == 1 => getter(bundle),
        _ => {}
    }
}

fn poster(mut bundle: Bundle) {
    let mut buf = vec![0; 1024 * 4];

    println!("got poster");

    while let Ok(n) = bundle.stream.read(&mut buf) {
        if n == 0 {
            break;
        }

        println!("received {} bytes", n);

        // SAFETY: No thread can panic while holding the lock
        let mut queue = bundle.queue.lock().unwrap();

        for b in &buf[..n] {
            queue.push_back(*b);
        }
    }

    println!("finished receiving");
}

fn getter(mut bundle: Bundle) {
    let mut buf = [0; 1];

    println!("got getter");

    match bundle.stream.read_exact(&mut buf) {
        Ok(()) => {
            let b = {
                let n = buf[0] as usize;
                let mut ret_buf = Vec::new();

                // SAFETY: No thread can panic while holding the lock
                let mut queue = bundle.queue.lock().unwrap();

                // Make sure enough bytes are present before trying to get them
                // TODO: Drop lock and retry retrieval
                if queue.len() >= n {
                    let bs = queue.drain(..n);
                    ret_buf.extend(bs);
                }

                ret_buf
            };
            println!("Sending {} bytes", b.len());

            // TODO: Need to handle stream writing potentially failing. Dropping connection is fine
            bundle.stream.write(&b).unwrap();
        }
        _ => {}
    }

    println!("end gottening");
}
