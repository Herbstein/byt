use std::{
    io::{self, Read, Write},
    net::TcpStream,
};

const N: usize = 8;

fn main() -> io::Result<()> {
    let mut stream = TcpStream::connect("127.0.0.1:8570")?;

    // Identify as "getter"
    stream.write(&[1])?;

    // Request a number of bytes
    stream.write(&[N as u8])?;
    stream.flush()?;

    // Read a single byte
    let mut buf = [0; N];
    stream.read_exact(&mut buf)?;

    println!("{:?}", buf);

    Ok(())
}
