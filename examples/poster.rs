use std::{
    io::{self, Write},
    net::TcpStream,
};

fn main() -> io::Result<()> {
    let mut stream = TcpStream::connect("127.0.0.1:8570")?;

    // Identify as "poster"
    stream.write(&[0])?;
    stream.flush()?;

    // Write random bytes, chosen with dice rolls
    stream.write(&[0x42, 0x43, 0x44, 0x45])?;
    stream.flush()?;

    Ok(())
}
